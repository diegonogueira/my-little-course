import play.api.{Mode, Configuration, GlobalSettings}
import java.io.File
import com.typesafe.config.ConfigFactory

object Global extends GlobalSettings {

  /**
   * Concat the environment application conf
   */

  override def onLoadConfig(config: Configuration, path: File, classloader: ClassLoader, mode: Mode.Mode): Configuration = {
    val configOverridePath = s"application.${mode.toString.toLowerCase}.conf"
    val modeSpecificConfig = config ++ Configuration(ConfigFactory.load(configOverridePath))
    super.onLoadConfig(modeSpecificConfig, path, classloader, mode)
  }

}