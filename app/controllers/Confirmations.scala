package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n.{Lang, Messages}
import models.User
import mailers.Mailer

object Confirmations extends Controller with Secured {

  val confirmationForm = Form(
    single(
      "email" -> email
    ).verifying(
      Messages("error.invalid_email_for_confirmation"),
        email => User.findByEmail(email).map { user =>
          !user.isConfirmed
        }.getOrElse(false)
    )
  )

  def resend_confirmation = NotAuthenticatedAction { implicit request =>
    Ok(views.html.confirmations.resend_confirmation(confirmationForm))
  }

  def resend = NotAuthenticatedAction { implicit request =>
    confirmationForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.confirmations.resend_confirmation(formWithErrors)),
      email => {
        User.findByEmail(email).map { user =>
          Mailer.send_user_confirmation_email(user.name, user.email, user.confirmation_token.get)
        }
        Redirect(routes.Home.index).flashing("notice" -> Messages("flash.confirmations.resend.notice"))
      }
    )
  }

  /**
   * Confirm user account form email link
   */

  def confirm(token: String) = NotAuthenticatedAction { implicit request =>
    User.findByConfirmationToken(token).map { user =>
      user.markAsConfirmed()
      Redirect(routes.Sessions.login).flashing("notice" -> Messages("flash.confirmations.confirm.notice"))
    }.getOrElse {
      Redirect(routes.Home.index).flashing("alert" -> Messages("error.confirmation_token_not_found"))
    }
  }

}
