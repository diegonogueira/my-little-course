package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n.{Lang, Messages}
import models.User
import mailers.Mailer
import lib.Utils

object ForgotPassword extends Controller with Secured {

  /**
   * Form to request a new password instructions
   */

  val forgotPasswordForm = Form(
    single(
      "email" -> email
    ).verifying(
      Messages("error.invalid_email_for_forgot_password"),
        email => User.findByEmail(email).map { user =>
          user.isConfirmed
        }.getOrElse(false)
    )
  )

  def forgotPassword = NotAuthenticatedAction { implicit request =>
    Ok(views.html.forgot_password.forgot_password(forgotPasswordForm))
  }

  def send = NotAuthenticatedAction { implicit request =>
    forgotPasswordForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.forgot_password.forgot_password(formWithErrors)),
      email => {
        User.findByEmail(email).map { user =>
          Mailer.send_forgot_password_email(user.name,
            user.email,
            user.generateForgotPasswordToken()
          )
        }
        Redirect(routes.Home.index).flashing("notice" -> Messages("flash.forgot_password.send.notice"))
      }
    )
  }

  /**
   * Form to change the new password
   */

  val resetPasswordForm = Form(
    tuple(
      "token" -> text.verifying(
        Messages("error.forgot_password_token_not_found"), User.findByForgotPasswordToken(_).isDefined
      ),
      "password" -> tuple(
        "main" -> text.verifying(
          pattern(Utils.PASSWORD_REGEXP, error=Messages("error.pattern_password"))
        ),
        "confirm" -> text
      ).verifying(
        Messages("error.passwords_do_not_match"), password => password._1 == password._2
      ).transform(
        { case (main, confirm) => main }, (main: String) => ("", "")
      )
    )
  )

  def resetPassword(token: String) = NotAuthenticatedAction { implicit request =>
    Ok(views.html.forgot_password.reset_password(
      resetPasswordForm.fill(token, "")
    ))
  }

  def update = NotAuthenticatedAction { implicit request =>
    resetPasswordForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.forgot_password.reset_password(formWithErrors)),
      params => {
        User.findByForgotPasswordToken(params._1).map { user =>
          user.updatePassword(params._2)
        }
        Redirect(routes.Home.index).flashing("notice" -> Messages("flash.forgot_password.update.notice"))
      }
    )
  }

}
