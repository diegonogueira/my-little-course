package controllers

import play.api._
import play.api.mvc._
import play.api.i18n.{Lang, Messages}

object Home extends Controller {

  def index = Action { implicit request =>
    Ok(views.html.home.index())
  }

}
