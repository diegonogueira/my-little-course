package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n.{Lang, Messages}
import mailers.Mailer
import models.User
import lib.Utils

object Registrations extends Controller with Secured {

  val userForm = Form(
  	tuple(
      "name" -> nonEmptyText,
      "email" -> email.verifying(
        Messages("error.email_already_exists"), User.findByEmail(_).isEmpty
      ),
      "password" -> tuple(
        "main" -> text.verifying(
          pattern(Utils.PASSWORD_REGEXP, error=Messages("error.pattern_password"))
        ),
        "confirm" -> text
      ).verifying(
        Messages("error.passwords_do_not_match"), password => password._1 == password._2
      ).transform(
        { case (main, confirm) => main }, (main: String) => ("", "")
      )
  	)
  )

  def signup = NotAuthenticatedAction { implicit request =>
    Ok(views.html.registrations.signup(userForm))
  }

  def registrate = NotAuthenticatedAction { implicit request =>
    userForm.bindFromRequest.fold(
    	formWithErrors => BadRequest(views.html.registrations.signup(formWithErrors)),
    	params => {
        User.registrate(params._1, params._2, params._3).map { user =>
          Mailer.send_user_confirmation_email(user.name, user.email, user.confirmation_token.get)
        }
    	  Redirect(routes.Home.index).flashing("notice" -> Messages("flash.registrations.registrate.notice"))
    	}
    )
  }

  def edit = AuthenticatedAction { implicit request =>
    Ok(views.html.registrations.edit("teste"))
  }
}
