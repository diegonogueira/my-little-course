package controllers

import play.api.mvc._
import play.api.mvc.Results._

trait Secured extends Controller {
  protected def userSession(request: RequestHeader) = request.session.get("id")
  private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Home.index)

  private def Authenticated(success: Request[AnyContent] => Result, failure: Request[AnyContent] => Result): Action[AnyContent] = {
    Action { request =>
      userSession(request).map { u => success(request) }.getOrElse { failure(request) }
    }
  }

  def AuthenticatedAction(f: Request[AnyContent] => Result): Action[AnyContent] = {
    Authenticated(f, onUnauthorized)
  }

  def NotAuthenticatedAction(f: Request[AnyContent] => Result): Action[AnyContent] = {
    Authenticated(onUnauthorized, f)
  }
}
