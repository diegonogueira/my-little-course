package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.i18n.{Lang, Messages}
import models.User

object Sessions extends Controller with Secured {

  val loginForm = Form(
    tuple(
      "email" -> nonEmptyText,
      "password" -> nonEmptyText
    ).verifying(
      Messages("error.invalid_email_or_passs"), result => User.authenticate(result._1, result._2)
    )
  )

  def login = NotAuthenticatedAction { implicit request =>
    Ok(views.html.sessions.login(loginForm))
  }

  def authenticate = NotAuthenticatedAction { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.sessions.login(formWithErrors)),
      params => {
        User.findByEmail(params._1).map { user =>
          Redirect(routes.Home.index).withSession("id" -> user.id.toString).flashing("notice" -> Messages("flash.sessions.authenticate.notice"))
        }.getOrElse(Redirect(routes.Home.index))
      }
    )
  }

  def logout = AuthenticatedAction { implicit request =>
    Redirect(routes.Home.index).withNewSession.flashing(
      "success" -> "You've been logged out"
    )
  }

}
