package helpers

import play.api.mvc._

object SecuredHelper {
  def userSignedIn(implicit request: RequestHeader) = request.session.get("id").isDefined
}