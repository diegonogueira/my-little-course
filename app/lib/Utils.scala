package lib

import org.mindrot.jbcrypt.BCrypt
import scala.util.Random

object Utils {

  val PASSWORD_REGEXP = """\A(?=.*[!@\#\$\*]{0,})(?=.*[0-9]{1,})(?=.*[a-z]{1,}).{8,}\z""".r

  def getCurrentDateTime = new java.sql.Timestamp(System.currentTimeMillis())

  def encrypt(pass: String, salt: Int = 12) = BCrypt.hashpw(pass, BCrypt.gensalt(salt))

  def checkCrypt(dec: String, enc: String) = BCrypt.checkpw(dec, enc)

  def randomAlphaNumericString(length: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
    randomStringFromCharList(length, chars)
  }

  def generateUniqueToken(length: Int = 60): String = randomAlphaNumericString(length)

  private def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    val sb = new StringBuilder
    for (i <- 1 to length) {
      val randomNum = util.Random.nextInt(chars.length)
      sb.append(chars(randomNum))
    }
    sb.toString
  }

}
