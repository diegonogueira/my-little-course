package mailers

import com.typesafe.plugin.MailerPlugin
import com.typesafe.plugin.use
import models.User
import play.api.Play.current
import play.api.i18n.{Lang, Messages}
import play.api.mvc._

object Mailer {

  val site_email = "My little course <contato@mylittlecourse.com>"

  /**
   * Send confirmation user email
   */

  def send_user_confirmation_email(name: String, email: String, token: String)(implicit request: RequestHeader) = {
    val mail = use[MailerPlugin].email
    mail.setFrom(site_email)
    mail.setRecipient(email)
    mail.setSubject(Messages("mailer.send_user_confirmation_email.subject"))
    mail.sendHtml(
      views.html.mailers.send_user_confirmation_email(name, email, token).toString
    )
  }

  /**
   * Send forgot password user email
   */

  def send_forgot_password_email(name: String, email: String, token: String)(implicit request: RequestHeader) = {
    val mail = use[MailerPlugin].email
    mail.setFrom(site_email)
    mail.setRecipient(email)
    mail.setSubject(Messages("mailer.send_forgot_password_email.subject"))
    mail.sendHtml(
      views.html.mailers.send_forgot_password_email(name, email, token).toString
    )
  }

}
