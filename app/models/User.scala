package models

import anorm._
import anorm.SqlParser._
import java.util.{Date}
import play.api.Play.current
import play.api.db._
import lib.Utils

case class User (
  id: Pk[Long],
  name: String,
  email: String,
  password: String,
  confirmation_token: Option[String],
  forgot_password_token: Option[String],
  about: Option[String],
  confirmed_at: Option[Date],
  created_at: Date,
  updated_at: Date
) {

  def isConfirmed = !confirmed_at.isEmpty

  def markAsConfirmed(): Int = DB.withConnection { implicit connection =>
    SQL("""
      UPDATE users SET confirmation_token = NULL, confirmed_at = {now} WHERE id = {id}
    """
    ).on(
      'id -> id,
      'now -> Utils.getCurrentDateTime
    ).executeUpdate()
  }

  def generateForgotPasswordToken(): String = DB.withConnection { implicit connection =>
    val token = Utils.generateUniqueToken()

    SQL("""
      UPDATE users SET forgot_password_token = {token} WHERE id = {id}
    """
    ).on(
      'id -> id,
      'token -> token
    ).executeUpdate()

    token
  }

  def updatePassword(new_password: String): Int = DB.withConnection { implicit connection =>
    SQL("""
      UPDATE users SET password = {password}, forgot_password_token = NULL WHERE id = {id}
    """
    ).on(
      'id -> id,
      'password -> Utils.encrypt(new_password)
    ).executeUpdate()
  }

}

object User {

  val full = {
    get[Pk[Long]]("users.id") ~
    get[String]("users.name") ~
    get[String]("users.email") ~
    get[String]("users.password") ~
    get[Option[String]]("users.confirmation_token") ~
    get[Option[String]]("users.forgot_password_token") ~
    get[Option[String]]("users.about") ~
    get[Option[Date]]("users.confirmed_at") ~
    get[Date]("users.created_at") ~
    get[Date]("users.updated_at") map {
      case id~name~email~password~confirmation_token~forgot_password_token~about~confirmed_at~created_at~updated_at =>
        User(id, name, email, password, confirmation_token, forgot_password_token, about, confirmed_at, created_at, updated_at)
    }
  }

  def count: Long = DB.withConnection { implicit c =>
    SQL("SELECT COUNT(*) AS total FROM users").apply().head[Long]("total")
  }

  def findById(id: Long): Option[User] = DB.withConnection { implicit c =>
    SQL("SELECT * FROM users WHERE id = {id} LIMIT 1").on('id -> id).as(User.full.singleOpt)
  }

  def findByEmail(email: String): Option[User] = DB.withConnection { implicit c =>
    SQL("SELECT * FROM users WHERE email = {email} LIMIT 1").on('email -> email).as(User.full.singleOpt)
  }

  def findByConfirmationToken(token: String): Option[models.User] = DB.withConnection { implicit c =>
    SQL("SELECT * FROM users WHERE confirmation_token = {token} LIMIT 1").on('token -> token).as(User.full.singleOpt)
  }

  def findByForgotPasswordToken(token: String): Option[models.User] = DB.withConnection { implicit c =>
    SQL("SELECT * FROM users WHERE forgot_password_token = {token} LIMIT 1").on('token -> token).as(User.full.singleOpt)
  }

  def registrate(name: String, email: String, password: String): Option[User] = DB.withConnection { implicit connection =>
    SQL("""
        INSERT INTO users (name, email, password, confirmation_token, created_at, updated_at) VALUES (
          {name}, {email}, {password}, {confirmation_token}, {created_at}, {updated_at}
        )
      """
    ).on(
      'name -> name,
      'email -> email,
      'password -> Utils.encrypt(password),
      'confirmation_token -> Utils.generateUniqueToken(),
      'created_at -> Utils.getCurrentDateTime,
      'updated_at -> Utils.getCurrentDateTime
    ).executeInsert().map { id =>
      findById(id)
    }.getOrElse(None)
  }

  def authenticate(email: String, password: String): Boolean = {
    findByEmail(email).map { user =>
      user.isConfirmed && Utils.checkCrypt(password, user.password)
    }.getOrElse(false)
  }
}
