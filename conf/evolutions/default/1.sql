# --- !Ups

CREATE TABLE users (
  id                   SERIAL       NOT NULL PRIMARY KEY,
  email                VARCHAR(255) NOT NULL UNIQUE,
  name                 VARCHAR(255) NOT NULL,
  password             VARCHAR(255) NOT NULL,
  confirmation_token   VARCHAR(255) UNIQUE,
  confirmed_at         TIMESTAMP,
  forgot_password_token VARCHAR(255) UNIQUE,
  admin                BOOLEAN      NOT NULL DEFAULT FALSE,
  about                TEXT,
  created_at           TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at           TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX users_email_idx ON users (email);
CREATE INDEX users_admin_idx ON users (admin);
CREATE UNIQUE INDEX users_confirmation_token_idx ON users (confirmation_token);
CREATE UNIQUE INDEX users_forgot_password_token_idx ON users (forgot_password_token);

# --- !Downs

DROP INDEX users_email_idx;
DROP INDEX users_admin_idx;
DROP INDEX users_confirmation_token_idx;
DROP INDEX users_forgot_password_token_idx;

DROP TABLE user;
