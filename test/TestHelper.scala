package test

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._

trait TestHelper {
  def fakeApp = FakeApplication(additionalConfiguration = inMemoryDatabase())

  def loginAs(email: String, pass: String="pass123456") = {
    route(FakeRequest(POST, "/authenticate").withFormUrlEncodedBody(
      ("email" -> email), ("password" -> pass)
    ))
  }
}


