package controllers

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import models.User
import fixtures.TestFixtures

class ConfirmationsSpec extends Specification with TestHelper {

  "the confirmations controller" should {

    /**
     * Resend confirmation FORM
     */

    "render resend confirmation" in running(fakeApp) {
      val result = route(FakeRequest(GET, "/resend_confirmation")).get

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
    }

    "not render resend confirmation when user is logged in" in running(fakeApp) {
      val request = FakeRequest(GET, "/resend_confirmation").withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Resend confirmation
     */

    "resend email confirmation" in running(fakeApp) {
      TestFixtures.populateUsers()

      route(FakeRequest(POST, "/confirmation/resend").withFormUrlEncodedBody(
        ("email" -> "tester2@test.com")
      )) must beSome.which { result =>
        status(result) must equalTo(SEE_OTHER)
        flash(result).get("notice") must beSome("Foi re-enviado um e-mail de confirmação. Por favor, abra o link para confirmar a sua conta.")
        redirectLocation(result) must beSome("/")
      }
    }

    "not resend email confirmation when user is confirmed" in running(fakeApp) {
      TestFixtures.populateUsers()

      route(FakeRequest(POST, "/confirmation/resend").withFormUrlEncodedBody(
        ("email" -> "tester1@test.com")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }
    }

    "not resend email confirmation when user is logeed in" in running(fakeApp) {
      TestFixtures.populateUsers()

      val request = FakeRequest(POST, "/confirmation/resend").withFormUrlEncodedBody(
        ("email" -> "tester2@test.net")
      ).withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Confirm
     */

    "confirm the user" in running(fakeApp) {
      TestFixtures.populateUsers()

      val token = User.findById(2).get.confirmation_token.get

      val result = route(FakeRequest(GET, s"/confirm/$token")).get

      flash(result).get("notice") must beSome("Sua conta foi confirmada com sucesso.")
      redirectLocation(result) must beSome("/login")
      User.findById(2).get.isConfirmed must beTrue
    }

    "redirect to home when token is invalid" in running(fakeApp) {
      val result = route(FakeRequest(GET, s"/confirm/123")).get
      flash(result).get("alert") must beSome("Este token de confirmação não é válido.")
      redirectLocation(result) must beSome("/")
    }

    "redirect to home when user is logged in" in running(fakeApp) {
      TestFixtures.populateUsers()

      val token = User.findById(2).get.confirmation_token.get

      val request = FakeRequest(GET, s"/confirm/$token").withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

  }
}
