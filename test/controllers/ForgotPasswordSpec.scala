package controllers

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import fixtures.TestFixtures
import models.User

class ForgotPasswordSpec extends Specification with TestHelper {

  "the passwords controller" should {

    /**
     * Forgot password FORM
     */

    "render forgot password" in running(fakeApp) {
      val result = route(FakeRequest(GET, "/forgot_password")).get

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
    }

    "not render forgot password when user is logged in" in running(fakeApp) {
      val request = FakeRequest(GET, "/forgot_password").withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Send password instructions
     */

    "send password" in running(fakeApp) {
      TestFixtures.populateUsers()

      route(FakeRequest(POST, "/forgot_password/send").withFormUrlEncodedBody(
        ("email" -> "tester1@test.com")
      )) must beSome.which { result =>
        status(result) must equalTo(SEE_OTHER)
        flash(result).get("notice") must beSome("Foi re-enviado um e-mail com as instruções para alteração de sua senha.")
        redirectLocation(result) must beSome("/")
      }
    }

    "not send password when user is not confirmed" in running(fakeApp) {
      TestFixtures.populateUsers()

      route(FakeRequest(POST, "/forgot_password/send").withFormUrlEncodedBody(
        ("email" -> "tester2@test.com")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }
    }

    "not send password when user is logeed in" in running(fakeApp) {
      TestFixtures.populateUsers()

      val request = FakeRequest(POST, "/forgot_password/send").withFormUrlEncodedBody(
        ("email" -> "tester1@test.net")
      ).withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Reset the password FORM
     */

    "render reset password" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(2).get
      val token = user.generateForgotPasswordToken()

      val result = route(FakeRequest(GET, s"/reset_password/$token")).get

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
    }

    "not render reset password when user is logged in" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(2).get
      val token = user.generateForgotPasswordToken()

      val request = FakeRequest(GET, s"/reset_password/$token").withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Update the user password
     */

    "update the user password" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(1).get
      val token = user.generateForgotPasswordToken()

      route(FakeRequest(POST, "/forgot_password/update").withFormUrlEncodedBody(
        ("token" -> token),
        ("password.main" -> "pass654321"),
        ("password.confirm" -> "pass654321")
      )) must beSome.which { result =>
        flash(result).get("notice") must beSome("Senha alterada com sucesso.")
        status(result) must equalTo(SEE_OTHER)
        redirectLocation(result) must beSome("/")
      }

      val result = route(FakeRequest(POST, "/authenticate").withFormUrlEncodedBody(
        ("email" -> "tester1@test.com"),
        ("password" -> "pass654321")
      )).get

      session(result).get("id") must beSome("1")
    }

    "not update the user password when user is logged in" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(1).get
      val token = user.generateForgotPasswordToken()

      val request = FakeRequest(POST, "/forgot_password/update").withFormUrlEncodedBody(
        ("password.main" -> "pass654321"),
        ("password.confirm" -> "pass654321")
      ).withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    "validate password" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(1).get
      val token = user.generateForgotPasswordToken()

      // not be blank
      route(FakeRequest(POST, "/forgot_password/update").withFormUrlEncodedBody(
        ("password.main" -> ""),
        ("password.confirm" -> "")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // is valid?
      route(FakeRequest(POST, "/forgot_password/update").withFormUrlEncodedBody(
        ("password.main" -> "654321"),
        ("password.confirm" -> "654321")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // confirmed?
      route(FakeRequest(POST, "/forgot_password/update").withFormUrlEncodedBody(
        ("password.main" -> "pass654321"),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

    }


  }
}
