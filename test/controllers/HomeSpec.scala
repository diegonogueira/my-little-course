package controllers

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper

class HomeSpec extends Specification with TestHelper {

  "the home controller" should {

    /**
     * Home index
     */

    "render index" in running(fakeApp) {
      val result = route(FakeRequest(GET, "/")).get

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
    }

  }
}
