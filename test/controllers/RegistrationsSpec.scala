package controllers

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import models.User
import fixtures.TestFixtures

class RegistrationsSpec extends Specification with TestHelper {

  "the registrations controller" should {

    /**
     * Sign up FORM
     */

    "render signup" in running(fakeApp) {
      val result = route(FakeRequest(GET, "/signup")).get

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
    }

    "not render signup when user is logged in" in running(fakeApp) {
      val request = FakeRequest(GET, "/signup").withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Registrate a new user
     */

    "registrate a new user" in running(fakeApp) {
      val result = route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> "tester@test.net"),
        ("password.main" -> "pass123456"),
        ("password.confirm" -> "pass123456")
      )).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
      flash(result).get("notice") must beSome("Uma mensagem com um link de confirmação foi enviada para o seu endereço de e-mail. Por favor, abra o link para confirmar a sua conta.")
      User.count must be equalTo(1)
    }

    "not registrate a new user when it is logged" in running(fakeApp) {
      val request = FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> "tester@test.net"),
        ("password.main" -> "pass123456"),
        ("password.confirm" -> "pass123456")
      ).withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Validations
     */

    "validate some fields" in running(fakeApp) {


      // name should not be blank
      route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> ""),
        ("email" -> "tester@test.net"),
        ("password.main" -> "pass123456"),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // email should not be blank
      route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> ""),
        ("password.main" -> "pass123456"),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // email should be uniq
      TestFixtures.populateUsers()
      route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> "tester1@test.com"),
        ("password.main" -> "pass123456"),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // email should have a valid format
      route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> "tester.com"),
        ("password.main" -> "pass123456"),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // password should not be blank
      route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> "tester@test.net"),
        ("password.main" -> ""),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // password should have a valid format
      route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
        ("name" -> "Tester"),
        ("email" -> "tester@test.net"),
        ("password.main" -> "123"),
        ("password.confirm" -> "pass123456")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }

      // password should be confirmed
      "not be valid when password confirmation is not equal to password" in running(fakeApp) {
        route(FakeRequest(POST, "/registrate").withFormUrlEncodedBody(
          ("name" -> "Tester"),
          ("email" -> "tester@test.net"),
          ("password.main" -> "pass123456"),
          ("password.confirm" -> "pass654321")
        )) must beSome.which { result =>
          status(result) must equalTo(BAD_REQUEST)
          contentType(result) must beSome("text/html")
        }
      }

    }


  }
}
