package controllers

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import models.User
import fixtures.TestFixtures

class SessionsSpec extends Specification with TestHelper {

  "the sessions controller" should {

    /**
     * Login FORM
     */

    "render login" in running(fakeApp) {
      val result = route(FakeRequest(GET, "/login")).get

      status(result) must equalTo(OK)
      contentType(result) must beSome("text/html")
      charset(result) must beSome("utf-8")
    }

    "not render login when user is logged in" in running(fakeApp) {
      val request = FakeRequest(GET, "/login").withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    /**
     * Authenticate
     */

    "authenticate an user" in running(fakeApp) {
      TestFixtures.populateUsers()

      val result = route(FakeRequest(POST, "/authenticate").withFormUrlEncodedBody(
        ("email" -> "tester1@test.com"),
        ("password" -> "pass123456")
      )).get

      session(result).get("id") must beSome("1")
      flash(result).get("notice") must beSome("Login efetuado com sucesso.")
      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    "not authenticate an user when it is logged in" in running(fakeApp) {
      val request = FakeRequest(POST, "/authenticate").withFormUrlEncodedBody(
        ("email" -> "tester1@test.com"),
        ("password" -> "pass123456")
      ).withSession("id" -> "1")
      val result = route(request).get

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must beSome("/")
    }

    "render signup and show authentication error message" in running(fakeApp) {
      TestFixtures.populateUsers()

      route(FakeRequest(POST, "/authenticate").withFormUrlEncodedBody(
        ("email" -> "tester1@test.com"),
        ("password" -> "123")
      )) must beSome.which { result =>
        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome("text/html")
      }
    }

    /**
     * Logout
     */

    "logout user" in running(fakeApp) {
      TestFixtures.populateUsers()
      loginAs("tester1@test.com")

      val result = route(FakeRequest(GET, "/logout")).get
      session(result).get("1") must beNone
      redirectLocation(result) must beSome("/")
    }

  }
}
