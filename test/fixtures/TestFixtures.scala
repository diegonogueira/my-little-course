package fixtures

import anorm.NotAssigned
import anorm.Id
import play.test.FakeApplication
import models.User

object TestFixtures {

  /**
   * Full populate
   */

  def populateAll() = {
    if (User.count == 0) {
      populateUsers()
    }
  }

  /**
   * Populate users
   */

  def populateUsers() = {
    // registrate the users
    User.registrate("Tester 1", "tester1@test.com", "pass123456")
    User.registrate("Tester 2", "tester2@test.com", "pass123456")

    // confirm some users
    val user = User.findById(1).get
    user.markAsConfirmed()
  }
}