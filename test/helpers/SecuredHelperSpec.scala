package userlogic

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import helpers.SecuredHelper

class SecuredHelperSpec extends Specification with TestHelper {

  "secured helper" should {

    "check if user is signed in" in running(fakeApp) {
      val withou_session_request = FakeRequest(GET, "/")

      SecuredHelper.userSignedIn(withou_session_request) must beFalse

      val with_session_request = FakeRequest(GET, "/").withSession("id" -> "1")

      SecuredHelper.userSignedIn(with_session_request) must beTrue
    }

  }

}