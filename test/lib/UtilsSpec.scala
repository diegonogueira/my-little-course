package lib

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import lib.Utils._

class UtilsSpec extends Specification with TestHelper {

  "password utils" should {

    "encrypt password" in running(fakeApp) {
      val pass = Utils.encrypt("pass123456")
      pass must have length(60)
    }

    "check password" in running(fakeApp) {
      val pass = Utils.encrypt("pass123456")

      Utils.checkCrypt("pass654321", pass) must beFalse
      Utils.checkCrypt("pass123456", pass) must beTrue
    }

  }

  "random utils" should {

    "generate a random alpha numeric string" in running(fakeApp) {
      val random_string = Utils.randomAlphaNumericString(20)
      random_string must have length(20)
    }

  }

  "token utils" should {

    "generate unique token string" in running(fakeApp) {
      val random_token = Utils.generateUniqueToken()
      random_token must have length(60)
    }

  }

}
