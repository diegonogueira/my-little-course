package models

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import test.TestHelper
import models.User._
import fixtures.TestFixtures

class UserSpec extends Specification with TestHelper {

  /**
   * User class
   */

  "user class" should {

    "check if user is confirmed" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(1).get
      user.isConfirmed must beTrue
    }

    "mark user as confirmed" in running(fakeApp) {
      TestFixtures.populateUsers()

      var user = User.findById(2).get

      user.isConfirmed must beFalse
      user.markAsConfirmed()

      user = User.findById(2).get
      user.confirmation_token must beNone
      user.confirmed_at must not be empty
      user.isConfirmed must beTrue
    }

    "generate a new forgot token password" in running(fakeApp) {
      TestFixtures.populateUsers()

      var user = User.findById(1).get
      user.forgot_password_token must be empty

      user.generateForgotPasswordToken()
      user = User.findById(1).get
      user.forgot_password_token.get must have length(60)
    }

  }

  /**
   * User object
   */

  "user object" should {

    "get total user count" in running(fakeApp) {
      User.count must beEqualTo(0)
    }

    "find user by id" in running(fakeApp) {
      User.findById(1) must beNone

      TestFixtures.populateUsers()

      val user = User.findById(1)
      user.get.id.toString must beEqualTo("1")
    }

    "find user by confirmation token" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(2).get

      User.findByConfirmationToken("xxx") must beNone
      User.findByConfirmationToken(user.confirmation_token.get).get must beEqualTo(user)
    }

    "find user by forgot password token" in running(fakeApp) {
      TestFixtures.populateUsers()

      val user = User.findById(2).get
      val token = user.generateForgotPasswordToken()

      User.findByForgotPasswordToken("xxx") must beNone
      User.findByForgotPasswordToken(token).get.id must beEqualTo(user.id)
    }

    "find user by email" in running(fakeApp) {
      User.findByEmail("teste1r@test.com") must beNone

      TestFixtures.populateUsers()

      val user = User.findByEmail("tester1@test.com")
      user.get.email must beEqualTo("tester1@test.com")
    }

    "check user authentication" in running(fakeApp) {
      TestFixtures.populateUsers()

      User.authenticate("tester1@test.com", "123") must beFalse
      User.authenticate("tester1@test.com", "pass123456") must beTrue
    }

    "registrate a new user" in running(fakeApp) {

      User.registrate("Tester", "tester@test.net", "pass123456")

      val user = User.findByEmail("tester@test.net").get

      user.name must beEqualTo("Tester")
      user.email must beEqualTo("tester@test.net")
      user.password must not be empty
      user.confirmation_token must not be empty
      user.created_at must not be empty
      user.updated_at must not be empty
    }

  }

}
